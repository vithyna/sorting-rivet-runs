# ProfVersion: 2.4.2
# Date: 2024-05-26 00:35:11
# InterpolationFile: /home/narendran/Documents/CEDAR_stuff2/sorting-rivet-runs/R5_Q2_2_300PDFs/ipol.dat
# DataDirectory: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/prof_refdata
#
# Limits:
#	PDF_APL  	3.009452 4.998966
#	PDF_BPL  	0.010940 2.999334
#	PDF_AHAD 	-0.498009 0.496158
#	PDF_BHAD 	-1.994193 -0.003474
#	PDF_AHADG	-0.498145 0.497239
#	PDF_BHADG	-2.499562 0.295795
#
# Fixed:
#
# Minimisation result:
#
# GOF 1091.387360
# UNITGOF 1091.387360
# NDOF 237.000000
PDF_APL  	3.175672
PDF_BPL  	2.513069
PDF_AHAD 	-0.304850
PDF_BHAD 	-0.686593
PDF_AHADG	0.307884
PDF_BHADG	-1.011143
#
# MIGRAD errors:
#
# PDF_APL  	5.561103e-03
# PDF_BPL  	1.586882e-02
# PDF_AHAD 	3.125412e-03
# PDF_BHAD 	9.859957e-03
# PDF_AHADG	5.574046e-03
# PDF_BHADG	2.928107e-02
#
# Correlation matrix:
#
#
#
# Weights used
#
# /H1_2002_I581409/d01-x01-y01      0.6   # 6 bins from 45 to 180
# /H1_2002_I581409/d02-x01-y01      0.6   # 4 bins from 20 to 80
# /H1_2002_I581409/d03-x01-y01      0.6   # 4 bins from 25 to 80
# /H1_2002_I581409/d04-x01-y01      0.6   # 5 bins from 0.6 to 2.5
# /H1_2002_I581409/d04-x01-y02      0.6   # 4 bins from 0.9 to 2.5
# /H1_2002_I581409/d05-x01-y01      0.6   # 6 bins from 0 to 2.5
# /H1_2002_I581409/d05-x01-y02      0.6   # 4 bins from 0.6 to 2.1
# /H1_2002_I581409/d06-x01-y01      0.8   # 3 bins from 0.5 to 1
# /H1_2002_I581409/d06-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d07-x01-y01      1.0   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d07-x01-y02      0.1   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d08-x01-y01      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d08-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d09-x01-y01      1.0   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d09-x01-y02      0.1   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d10-x01-y01      1.0   # 8 bins from 0 to 0.85
# /H1_2002_I581409/d10-x01-y02      0.1   # 8 bins from 0 to 0.85
# /ZEUS_1997_I450085/d01-x01-y01    0.5   # 11 bins from -1.125 to 1.625
# /ZEUS_1997_I450085/d02-x01-y01    0.5   # 10 bins from -0.875 to 1.625
# /ZEUS_1997_I450085/d03-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d04-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d21-x01-y01    0.1   # 9 bins from -1.125 to 1.125
# /ZEUS_1997_I450085/d22-x01-y01    0.1   # 8 bins from -0.875 to 1.125
# /ZEUS_1997_I450085/d23-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d24-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d25-x01-y01    1.0   # 8 bins from -0.375 to 1.5
# /ZEUS_1997_I450085/d26-x01-y01    1.0   # 8 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d27-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_1997_I450085/d28-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_2012_I1116258/d01-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d02-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d03-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d04-x01-y01   0.6   # 5 bins from 17 to 41
# /ZEUS_2012_I1116258/d05-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d06-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d07-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d08-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d09-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d10-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d11-x01-y01   0.0   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d12-x01-y01   0.0   # 13 bins from -0.75 to 2.5
#
# Mean contribution:
#
# /H1_2002_I581409/d01-x01-y01	2.346056
# /H1_2002_I581409/d02-x01-y01	0.920339
# /H1_2002_I581409/d03-x01-y01	0.909313
# /H1_2002_I581409/d04-x01-y01	0.590215
# /H1_2002_I581409/d04-x01-y02	0.244780
# /H1_2002_I581409/d05-x01-y01	2.533784
# /H1_2002_I581409/d05-x01-y02	1.057773
# /H1_2002_I581409/d06-x01-y01	10.037915
# /H1_2002_I581409/d06-x01-y02	12.614318
# /H1_2002_I581409/d07-x01-y01	12.025956
# /H1_2002_I581409/d07-x01-y02	0.022064
# /H1_2002_I581409/d08-x01-y01	12.536005
# /H1_2002_I581409/d08-x01-y02	4.908234
# /H1_2002_I581409/d09-x01-y01	7.423910
# /H1_2002_I581409/d09-x01-y02	0.016392
# /H1_2002_I581409/d10-x01-y01	16.562916
# /H1_2002_I581409/d10-x01-y02	0.008338
# /ZEUS_1997_I450085/d01-x01-y01	3.239418
# /ZEUS_1997_I450085/d02-x01-y01	0.859587
# /ZEUS_1997_I450085/d03-x01-y01	0.736305
# /ZEUS_1997_I450085/d04-x01-y01	0.959850
# /ZEUS_1997_I450085/d21-x01-y01	0.005393
# /ZEUS_1997_I450085/d22-x01-y01	0.006051
# /ZEUS_1997_I450085/d23-x01-y01	0.004927
# /ZEUS_1997_I450085/d24-x01-y01	0.001202
# /ZEUS_1997_I450085/d25-x01-y01	12.786796
# /ZEUS_1997_I450085/d26-x01-y01	4.992462
# /ZEUS_1997_I450085/d27-x01-y01	5.193941
# /ZEUS_1997_I450085/d28-x01-y01	6.425471
# /ZEUS_2012_I1116258/d01-x01-y01	4.448457
# /ZEUS_2012_I1116258/d02-x01-y01	7.478339
# /ZEUS_2012_I1116258/d03-x01-y01	5.479576
# /ZEUS_2012_I1116258/d04-x01-y01	3.966080
# /ZEUS_2012_I1116258/d05-x01-y01	5.856092
# /ZEUS_2012_I1116258/d06-x01-y01	3.680185
# /ZEUS_2012_I1116258/d07-x01-y01	6.794829
# /ZEUS_2012_I1116258/d08-x01-y01	1.978048
