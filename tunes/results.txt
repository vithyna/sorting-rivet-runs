# ProfVersion: 2.4.2
# Date: 2024-05-24 18:50:15
# InterpolationFile: /home/narendran/Documents/CEDAR_stuff2/sorting-rivet-runs/ipol.dat
# DataDirectory: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/prof_refdata
#
# Limits:
#	PDF_APL  	3.005060 4.995414
#	PDF_BPL  	0.006636 2.993577
#	PDF_AHAD 	-0.498579 0.491711
#	PDF_BHAD 	-1.999007 -0.016610
#	PDF_AHADG	-0.498870 0.498739
#	PDF_BHADG	-2.496838 0.298679
#
# Fixed:
#
# Minimisation result:
#
# GOF 192.618272
# UNITGOF 192.618272
# NDOF 237.000000
PDF_APL  	4.105029
PDF_BPL  	2.956984
PDF_AHAD 	-0.365871
PDF_BHAD 	-0.381601
PDF_AHADG	0.271270
PDF_BHADG	-1.539427
#
# MIGRAD errors:
#
# PDF_APL  	1.498663e-01
# PDF_BPL  	3.152114e-02
# PDF_AHAD 	2.378986e-02
# PDF_BHAD 	6.114880e-02
# PDF_AHADG	3.266277e-02
# PDF_BHADG	1.061283e-01
#
# Correlation matrix:
#
#
#
# Weights used
#
# /H1_2002_I581409/d01-x01-y01      0.6   # 6 bins from 45 to 180
# /H1_2002_I581409/d02-x01-y01      0.6   # 4 bins from 20 to 80
# /H1_2002_I581409/d03-x01-y01      0.6   # 4 bins from 25 to 80
# /H1_2002_I581409/d04-x01-y01      0.6   # 5 bins from 0.6 to 2.5
# /H1_2002_I581409/d04-x01-y02      0.6   # 4 bins from 0.9 to 2.5
# /H1_2002_I581409/d05-x01-y01      0.6   # 6 bins from 0 to 2.5
# /H1_2002_I581409/d05-x01-y02      0.6   # 4 bins from 0.6 to 2.1
# /H1_2002_I581409/d06-x01-y01      0.8   # 3 bins from 0.5 to 1
# /H1_2002_I581409/d06-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d07-x01-y01      1.0   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d07-x01-y02      0.1   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d08-x01-y01      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d08-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d09-x01-y01      1.0   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d09-x01-y02      0.1   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d10-x01-y01      1.0   # 8 bins from 0 to 0.85
# /H1_2002_I581409/d10-x01-y02      0.1   # 8 bins from 0 to 0.85
# /ZEUS_1997_I450085/d01-x01-y01    0.5   # 11 bins from -1.125 to 1.625
# /ZEUS_1997_I450085/d02-x01-y01    0.5   # 10 bins from -0.875 to 1.625
# /ZEUS_1997_I450085/d03-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d04-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d21-x01-y01    0.1   # 9 bins from -1.125 to 1.125
# /ZEUS_1997_I450085/d22-x01-y01    0.1   # 8 bins from -0.875 to 1.125
# /ZEUS_1997_I450085/d23-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d24-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d25-x01-y01    1.0   # 8 bins from -0.375 to 1.5
# /ZEUS_1997_I450085/d26-x01-y01    1.0   # 8 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d27-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_1997_I450085/d28-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_2012_I1116258/d01-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d02-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d03-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d04-x01-y01   0.6   # 5 bins from 17 to 41
# /ZEUS_2012_I1116258/d05-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d06-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d07-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d08-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d09-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d10-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d11-x01-y01   0.0   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d12-x01-y01   0.0   # 13 bins from -0.75 to 2.5
#
# Mean contribution:
#
# /H1_2002_I581409/d01-x01-y01	0.246366
# /H1_2002_I581409/d02-x01-y01	0.092740
# /H1_2002_I581409/d03-x01-y01	0.050676
# /H1_2002_I581409/d04-x01-y01	0.233961
# /H1_2002_I581409/d04-x01-y02	0.618585
# /H1_2002_I581409/d05-x01-y01	0.181985
# /H1_2002_I581409/d05-x01-y02	0.077510
# /H1_2002_I581409/d06-x01-y01	2.414119
# /H1_2002_I581409/d06-x01-y02	2.364767
# /H1_2002_I581409/d07-x01-y01	2.075903
# /H1_2002_I581409/d07-x01-y02	0.060471
# /H1_2002_I581409/d08-x01-y01	2.206140
# /H1_2002_I581409/d08-x01-y02	1.313680
# /H1_2002_I581409/d09-x01-y01	1.376333
# /H1_2002_I581409/d09-x01-y02	0.052696
# /H1_2002_I581409/d10-x01-y01	5.451253
# /H1_2002_I581409/d10-x01-y02	0.023050
# /ZEUS_1997_I450085/d01-x01-y01	0.168715
# /ZEUS_1997_I450085/d02-x01-y01	0.112673
# /ZEUS_1997_I450085/d03-x01-y01	0.120043
# /ZEUS_1997_I450085/d04-x01-y01	0.202661
# /ZEUS_1997_I450085/d21-x01-y01	0.006434
# /ZEUS_1997_I450085/d22-x01-y01	0.002534
# /ZEUS_1997_I450085/d23-x01-y01	0.002428
# /ZEUS_1997_I450085/d24-x01-y01	0.007083
# /ZEUS_1997_I450085/d25-x01-y01	0.446802
# /ZEUS_1997_I450085/d26-x01-y01	0.563141
# /ZEUS_1997_I450085/d27-x01-y01	0.657953
# /ZEUS_1997_I450085/d28-x01-y01	0.528088
# /ZEUS_2012_I1116258/d01-x01-y01	1.298275
# /ZEUS_2012_I1116258/d02-x01-y01	1.633448
# /ZEUS_2012_I1116258/d03-x01-y01	0.348449
# /ZEUS_2012_I1116258/d04-x01-y01	0.789526
# /ZEUS_2012_I1116258/d05-x01-y01	1.186688
# /ZEUS_2012_I1116258/d06-x01-y01	0.657960
# /ZEUS_2012_I1116258/d07-x01-y01	0.493758
# /ZEUS_2012_I1116258/d08-x01-y01	0.656202
